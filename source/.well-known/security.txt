Contact: security@gitlab.com

Disclosure policy: https://about.gitlab.com/disclosure/

Acknowledgements: https://about.gitlab.com/vulnerability-acknowledgements/
