# Data schemas

## Team Members

### [`team.yml`](./team.yml)

#### Common Properties

| Property     | Required | Example        | Description |
|--------------|----------|----------------|-------------|
| `slug`       | `true`   | `dz` or `cto`  | A human-readable unique id based on the person's name or a vacancy's title. For use with `reports_to` |
| `type`       | `true`   | `person`       | Must be `person` or `vacancy` |
| `start_date` | `true`   | `2018-01-01`   | An iso date of when the person started, will start, or when a vacancy is intended to be filled |
| `reports_to` | `true`   | `sid`, `null`  | A person slug, or an explicit null if the person is top level (like the board) |
| `role_name`  | `true`   | `CTO, Founder` | Text of the person's title. Will be hyperlinked with `role_path` in the template(s) |
| `role_path`  | `true`   | `/roles/cto/`  | Relative path on the site for the person's role page. Used to build hyperlinks in templates |
| `function`   | `true`   | `Engineering`  | Text of someone's function. Required if a `department` is set |
| `department` | `false`  | `Dev Backend`  | Text of someone's department. Required if a `team` is set` |
| `team`       | `false`  | `Discussion`   | Text of someone's team |

#### Person-specific Properties

| Property     | Required     | Example        | Description |
|--------------|--------------|----------------|-------------|
| `first_name` | `true`       | `Eric`         | Text of the person's first name. If the start_date is in the future, the template should render the first letter only followed by a period |
| `nick_name`  | `false`      | `EJ`           | Text of the person's optional last name rendered in the template in quotes between the first and last names.  If the start_date is in the future, the template should not render this at all |
| `last_name`  | `true`       | `Johnson`      | Text of the person's last name. If the start_date is in the future, the template should render the first letter only followed by a period |
| `twitter`    | `false`      | `dzaporozhets` | A Twitter handle (without the @ symbol) |
| `gitlab`     | `false`      | `dzaporozhets` | A GitLab.com handle (without the @ symbol) |
| `picture`    | `false`      | `dmitriy.png`  | The filename of a picture in `/images/` (the template should default to it's own image if not specified) |
| `locality`   | `false`      | `Kharkiv`      | The text location within a country of the person |
| `country`    | `true`       | `Ukraine`      | The text of the country of the person |
| `projects`   | `false`      | [object]       | Array of values from `projects.yml` |
| `expertise`  | `false`      | `<b>HTML</b>`  | Text of the person's expertises |
| `story`      | `false`      | `Some string`  | Text of the person's biography |

#### Vacancy-specific Properties

| Property     | Required     | Example           | Description |
|--------------|--------------|-------------------|-------------|
| `lever_url`  | `true`       | `https://...`     | The fully qualified URL of the vacancy from our applicant tracking system |
| `recruiter`  | `true`       | `Steve Pestorich` | The text name of the recruiter |
| `interim`    | `false`      | `Stan H`          | The text name of an interim individual filling the role. Required if someone reports to this role? |

## Roles

### [`roles.yml`](./roles.yml)

#### Properties

| Property        | Required | Example                            | Description |
|-----------------|----------|------------------------------------|-------------|
| `title`         | `true`   | `Accountant` or `Developer`        | A human-friendly title for the role. |
| `description`   | `true`   | `/job-families/finance/accountant` | The relative URI to the role page in the handbook. Used to load the compensation calculator on the correct page. |
| `levels`        | `true`   | `basic` or `false`               | If `false`, indicates that this role does not have levels. Otherwise, references an entry in the role_levels.yml file. Use `basic` if you are not sure. |
| `salary`        | `true`   | `100000`                           | The San Francisco benchmark for this position. Used in the compensation calculator. |
| `salary source` | `false`  | `Aventador` or `E-team`            | Attributes the source of the benchmark defined in `salary`. Not used by code. |

### [`role_levels.yml`](./role_levels.yml)

#### Properties

| Property     | Required | Example                     | Description |
|--------------|----------|-----------------------------|-------------|
| `title`      | `true`   | `Junior` or `Intermediate`  | The human-readable title for the role level. Displayed in the compensation calculator. |
| `factor`     | `true`   | 1.0                         | The multiplier used to calculate compensation for this level |
| `default`    | `false`   | true   | A boolean indicating whether this level is shown by default in the calculator. |
